let baseURL = 'https://gdvqwkms.lc-cn-n1-shared.com'
export const $http = function(url, method = "GET", data = {}) {
	return new Promise((resolve, reject) => {
		uni.request({
			url: baseURL + url,
			method,
			data,
			header:{
				"X-LC-Id": "gDvQwkMs3tXHLJb2nNwlzgNo-gzGzoHsz",
				"X-LC-Key": "K3mvzuuygqeJJMnw6PexHwTb",
				// "Content-Type": "application/json"
			},
			success: (res) => {
				resolve(res.data)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}

export const $get = function(url,data={}){
	return $http(url,'GET',data)
}
export const $post = function(url,data={}){
	return $http(url,'POST',data)
}
export const $put = function(url,data={}){
	return $http(url,'PUT',data)
}
export const $delete = function(url,data={}){
	return $http(url,'DELETE',data)
}
