import Vue from 'vue'
import Vuex from 'vuex'
import condition from './condition.js'
import user from './user.js'
import detail from './detail.js'
import cart from './cart.js'
import address from './address.js'
Vue.use(Vuex)
const store = new Vuex.Store({
	modules:{
		condition,
		user,
		detail,
		cart,
		address
	}
})
export default store